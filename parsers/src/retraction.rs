// Copyright (c) 2023 Werner Kroneman
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

use crate::message::MessagePayload;

generate_element!(
    /// Denotes a message to be retracted.
    /// https://xmpp.org/extensions/xep-0424.html
    Retract, "retract", RETRACTIONS,
    attributes: [
        /// The 'id' attribute of the received message.
        id: Required<String> = "id",
    ]
);

impl MessagePayload for Retract {}

generate_element!(
    /// Notes that a message with the given id has been retracted.
    /// This is the tombstone version of the retraction:
    /// https://xmpp.org/extensions/xep-0424.html#tombstones
    Retracted, "retracted", RETRACTIONS,
    attributes: [
        /// The 'id' attribute of the received message.
        id: Required<String> = "id",
        /// The time at which the retraction took place.
        stamp: Option<String> = "stamp",
    ]
);

impl MessagePayload for Retracted {}
