// Copyright (c) XMPP-RS Contributors
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

//! A collection of various enums that are relevant to PubSub.

use std::str::FromStr;

/// Access model options according to:
/// https://xmpp.org/extensions/xep-0060.html#table-6
pub enum AccessModel {
    /// Any entity may subscribe to the node.
    Open,
    /// TODO How does that work?
    Presence,
    /// TODO How does that work?
    Roster,
    /// Node owner must approve subscription requests.
    Authorize,
    /// Only whitelisted entities may subscribes.
    Whitelist,
}

impl ToString for AccessModel {
    fn to_string(&self) -> String {
        match self {
            AccessModel::Open => "open",
            AccessModel::Presence => "presence",
            AccessModel::Roster => "roster",
            AccessModel::Authorize => "authorize",
            AccessModel::Whitelist => "whitelist",
        }
        .to_string()
    }
}

/// An invalid string was given as an enum constant.
#[derive(Debug, Clone, Copy)]
pub struct InvalidOption;

impl FromStr for AccessModel {
    type Err = InvalidOption;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        match s {
            "open" => Ok(AccessModel::Open),
            "presence" => Ok(AccessModel::Presence),
            "roster" => Ok(AccessModel::Roster),
            "authorize" => Ok(AccessModel::Authorize),
            "whitelist" => Ok(AccessModel::Whitelist),
            _ => Err(InvalidOption),
        }
    }
}

/// The sending behavior when subscribing to a given pubsub node.
pub enum SendLastPublished {
    /// Do not auto-send.
    Never,
    /// Send on subscription.
    OnSub,
    /// Send on subscription and on presence.
    OnSubAndPresence,
}

impl ToString for SendLastPublished {
    fn to_string(&self) -> String {
        match self {
            SendLastPublished::Never => "never",
            SendLastPublished::OnSub => "on_sub",
            SendLastPublished::OnSubAndPresence => "on_sub_and_presence",
        }
        .to_string()
    }
}

impl FromStr for SendLastPublished {
    type Err = InvalidOption;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        match s {
            "never" => Ok(SendLastPublished::Never),
            "on_sub" => Ok(SendLastPublished::OnSub),
            "on_sub_and_presence" => Ok(SendLastPublished::OnSubAndPresence),
            _ => Err(InvalidOption),
        }
    }
}
