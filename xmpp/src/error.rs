use crate::parsers::iq::Iq;
use crate::parsers::message::Message;
use crate::parsers::stanza_error::{DefinedCondition, ErrorType, StanzaError};
use crate::Element;

/// Create a response <iq/> stanza with a <bad-request/> error of error type "modify".
/// This is an appropriate response to a message that is certainly malformed.
///
/// # Arguments
///
/// - `original`: The original message that is being responded to.
/// - `error_text`: The error text to include in the <bad-request/> stanza.
pub fn mk_bad_request_stanza(original: &Message, error_text: String) -> Element {
    let mut e = Iq::from_error(
        // TODO: Not quite sure what to do if the id is missing entirely.
        // Should this be optional on the parser instead, since we're
        // likely dealing with malformed messages?
        original.id.clone().unwrap_or("".to_string()),
        StanzaError::new(
            ErrorType::Modify,
            DefinedCondition::BadRequest,
            "en",
            error_text,
        ),
    );

    if let Some(from) = &original.from {
        e = e.with_from(from.clone())
    }

    e.into()
}
