use crate::parsers::message::{Message, MessageType};
use crate::parsers::ns::MUC_USER;
use crate::{BareJid, Jid, RoomNick};
use std::fmt::Display;

/// The context in which an incoming message was received.
#[derive(PartialEq, Debug, Clone)]
pub enum InferredMessageContext {
    /// This is a direct message from one user to another.
    Direct { with_other: BareJid },
    /// This is a message in a MUC room, sent privately from one member to another.
    MucPrivate {
        in_room: BareJid,
        with_member: RoomNick,
    },
    /// This is a message in a MUC room, sent publicly to all members.
    MucPublic { in_room: BareJid },
}

/// An error that can occur when inferring the context of an incoming message.
#[derive(PartialEq, Debug, Clone)]
pub enum MessageContextInferError {
    /// The message has no 'from' attribute.
    NoFromAttribute,
}

impl Display for MessageContextInferError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            MessageContextInferError::NoFromAttribute => {
                write!(f, "Message has no 'from' attribute")
            }
        }
    }
}

/// Infer the context of an incoming message based on its type and sender.
///
/// For reference, see: https://xmpp.org/extensions/xep-0045.html#message
pub fn infer_incoming_message_context(
    message: &Message,
) -> Result<InferredMessageContext, MessageContextInferError> {
    let from = message
        .from
        .as_ref()
        .ok_or(MessageContextInferError::NoFromAttribute)?;

    let inferred = match (&message.type_, &from) {
        // If it's a Chat/Normal message originating from a bare JID, it's a direct message.
        (MessageType::Chat | MessageType::Normal, Jid::Bare(jid)) => {
            InferredMessageContext::Direct {
                with_other: jid.clone(),
            }
        }
        // If it's a GroupChat message, it's a public Muc message
        (MessageType::Groupchat, jid) => {
            // Unpack the JID
            InferredMessageContext::MucPublic {
                in_room: jid.to_bare(),
            }
        }
        // If it's a Chat/Normal message originating from a full JID...
        (MessageType::Chat | MessageType::Normal, Jid::Full(jid)) => {
            // Check if the payloads include <x xmlns='http://jabber.org/protocol/muc#user' />
            if message.payloads.iter().any(|p| p.is("x", MUC_USER)) {
                // If so, it's a private MUC message
                InferredMessageContext::MucPrivate {
                    in_room: jid.to_bare(),
                    with_member: jid.resource_str().to_owned(),
                }
            } else {
                // Otherwise, it's a direct message
                InferredMessageContext::Direct {
                    with_other: jid.to_bare(),
                }
            }
        }
        (t, f) => panic!("Unexpected message type: {:?} and sender {:?}", t, f),
    };

    Ok(inferred)
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::parsers::message::Message;
    use crate::Element;
    use std::str::FromStr;

    #[test]
    fn muc() {
        // From https://xmpp.org/extensions/xep-0045.html#example-45
        let message = r#"<message xmlns='jabber:client'
    from='coven@chat.shakespeare.lit/thirdwitch'
    id='hysf1v37'
    to='crone1@shakespeare.lit/desktop'
    type='groupchat'>
  <body>Harpier cries: 'tis time, 'tis time.</body>
</message>"#;

        let message = Message::try_from(Element::from_str(message).unwrap()).unwrap();

        assert_eq!(
            infer_incoming_message_context(&message),
            Ok(InferredMessageContext::MucPublic {
                in_room: BareJid::from_str("coven@chat.shakespeare.lit").unwrap(),
            })
        );
    }

    #[test]
    fn muc_private() {
        // From https://xmpp.org/extensions/xep-0045.html#example-47
        let message = r"<message xmlns='jabber:client'
    from='coven@chat.shakespeare.lit/secondwitch'
    id='hgn27af1'
    to='crone1@shakespeare.lit/desktop'
    type='chat'>
  <body>I'll give thee a wind.</body>
  <x xmlns='http://jabber.org/protocol/muc#user' />
</message>";

        let message = Message::try_from(Element::from_str(message).unwrap()).unwrap();

        assert_eq!(
            infer_incoming_message_context(&message),
            Ok(InferredMessageContext::MucPrivate {
                in_room: BareJid::from_str("coven@chat.shakespeare.lit").unwrap(),
                with_member: "secondwitch".to_owned(),
            })
        );
    }

    #[test]
    fn test_direct() {
        // From https://xmpp.org/rfcs/rfc6121.html#message-syntax-type
        let message = r#"<message xmlns='jabber:client'
    from='juliet@example.com/balcony'
    id='b4vs9km4'
    to='romeo@example.net'
    type='chat'
    xml:lang='en'>
  <body>Wherefore art thou, Romeo?</body>
</message>"#;

        let message = Message::try_from(Element::from_str(message).unwrap()).unwrap();

        assert_eq!(
            infer_incoming_message_context(&message),
            Ok(InferredMessageContext::Direct {
                with_other: BareJid::from_str("juliet@example.com").unwrap(),
            })
        );
    }

    #[test]
    fn test_direct_notype() {
        // From https://xmpp.org/rfcs/rfc6121.html#message-syntax-type
        let message = r#"<message xmlns='jabber:client'
   from='juliet@example.com/balcony'
    id='b4vs9km4'
    to='romeo@example.net'
    xml:lang='en'>
  <body>Wherefore art thou, Romeo?</body>
</message>"#;

        let message = Message::try_from(Element::from_str(message).unwrap()).unwrap();

        assert_eq!(
            infer_incoming_message_context(&message),
            Ok(InferredMessageContext::Direct {
                with_other: BareJid::from_str("juliet@example.com").unwrap(),
            })
        );
    }
}
