// Copyright (c) 2019 Emmanuel Gil Peyrot <linkmauve@linkmauve.fr>
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

use super::Agent;
use crate::parsers::iq::Iq;
use crate::parsers::ns::BOOKMARKS2;
use crate::parsers::pubsub::enums::SendLastPublished;
use crate::parsers::pubsub::pubsub::PublishOptions;
use crate::parsers::pubsub::PubSub::Publish;
use crate::parsers::pubsub::{ItemId, NodeName};
use crate::{parsers, Event, RoomNick};
use std::str::FromStr;
use tokio_xmpp::parsers::pubsub::enums::AccessModel;
use tokio_xmpp::parsers::{
    bookmarks2::{self, Autojoin},
    ns,
    pubsub::event::PubSubEvent,
    pubsub::pubsub::PubSub,
    BareJid, Element, Jid,
};

#[cfg(feature = "avatars")]
pub(crate) mod avatar;

pub(crate) async fn handle_event(from: &Jid, elem: Element, agent: &mut Agent) -> Vec<Event> {
    let mut events = Vec::new();
    let event = PubSubEvent::try_from(elem);
    trace!("PubSub event: {:#?}", event);
    match event {
        Ok(PubSubEvent::PublishedItems { node, items }) => {
            match node.0 {
                #[cfg(feature = "avatars")]
                ref node if node == ns::AVATAR_METADATA => {
                    let new_events =
                        avatar::handle_metadata_pubsub_event(&from, agent, items).await;
                    events.extend(new_events);
                }
                ref node if node == ns::BOOKMARKS2 => {
                    // TODO: Check that our bare JID is the sender.
                    assert_eq!(items.len(), 1);
                    let item = items.clone().pop().unwrap();
                    let jid = BareJid::from_str(&item.id.clone().unwrap().0).unwrap();
                    let payload = item.payload.clone().unwrap();
                    match bookmarks2::Conference::try_from(payload) {
                        Ok(conference) => {
                            if conference.autojoin == Autojoin::True {
                                events.push(Event::JoinRoom(jid, conference));
                            } else {
                                events.push(Event::LeaveRoom(jid));
                            }
                        }
                        Err(err) => println!("not bookmark: {}", err),
                    }
                }
                ref node => unimplemented!("node {}", node),
            }
        }
        Ok(PubSubEvent::RetractedItems { node, items }) => {
            match node.0 {
                ref node if node == ns::BOOKMARKS2 => {
                    // TODO: Check that our bare JID is the sender.
                    assert_eq!(items.len(), 1);
                    let item = items.clone().pop().unwrap();
                    let jid = BareJid::from_str(&item.0).unwrap();
                    events.push(Event::LeaveRoom(jid));
                }
                ref node => unimplemented!("node {}", node),
            }
        }
        Ok(PubSubEvent::Purge { node }) => {
            match node.0 {
                ref node if node == ns::BOOKMARKS2 => {
                    // TODO: Check that our bare JID is the sender.
                    events.push(Event::LeaveAllRooms);
                }
                ref node => unimplemented!("node {}", node),
            }
        }
        Err(e) => {
            error!("Error parsing PubSub event: {}", e);
        }
        _ => unimplemented!("PubSub event: {:#?}", event),
    }
    events
}

pub(crate) fn handle_iq_result(from: &Jid, elem: Element) -> impl IntoIterator<Item = Event> {
    let mut events = Vec::new();
    let pubsub = PubSub::try_from(elem).unwrap();
    trace!("PubSub: {:#?}", pubsub);
    if let PubSub::Items(items) = pubsub {
        match items.node.0.clone() {
            #[cfg(feature = "avatars")]
            ref node if node == ns::AVATAR_DATA => {
                let new_events = avatar::handle_data_pubsub_iq(&from, &items);
                events.extend(new_events);
            }
            ref node if node == ns::BOOKMARKS2 => {
                events.push(Event::LeaveAllRooms);
                for item in items.items {
                    let item = item.0;
                    let jid = BareJid::from_str(&item.id.clone().unwrap().0).unwrap();
                    let payload = item.payload.clone().unwrap();
                    match bookmarks2::Conference::try_from(payload) {
                        Ok(conference) => {
                            if let Autojoin::True = conference.autojoin {
                                events.push(Event::JoinRoom(jid, conference));
                            }
                        }
                        Err(err) => panic!("Wrong payload type in bookmarks 2 item: {}", err),
                    }
                }
            }
            _ => unimplemented!(),
        }
    }
    events
}

/// Create the <iq> stanza that will create a new room bookmark.
///
/// # Arguments
/// * room: The Jid of the room itself.
/// * auto_join: Whether to auto-join on client startup
/// * nickname: Our nickname within the room, if applicable.
/// * password: The password of the room, if applicable.
pub fn mk_create_bookmark_iq(
    room: &BareJid,
    auto_join: bool,
    nickname: Option<RoomNick>,
    password: Option<String>,
) -> Iq {
    // Create an Iq stanza that specifies that we wish to set a bookmark.
    let bookmark = bookmarks2::Conference {
        autojoin: if auto_join {
            Autojoin::True
        } else {
            Autojoin::False
        },
        name: Some(room.to_string()),
        nick: nickname,
        password,
        extensions: vec![],
    };

    let item = parsers::pubsub::Item {
        id: Some(ItemId(room.to_string())),
        publisher: None,
        payload: Some(bookmark.into()),
    };

    let iq = Iq::from_set(
        "create-bookmark",
        // We'd like to publish a new item.
        Publish {
            // What we would like to publish.
            publish: parsers::pubsub::pubsub::Publish {
                // Put us into the 0402 bookmarks namespace
                node: NodeName(BOOKMARKS2.to_string()),
                items: vec![parsers::pubsub::pubsub::Item(item)],
            },
            publish_options: Some(PublishOptions::new(
                true,
                None,
                SendLastPublished::Never,
                AccessModel::Whitelist,
            )),
        },
    );

    iq
}
