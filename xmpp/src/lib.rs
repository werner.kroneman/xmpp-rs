// Copyright (c) 2019 Emmanuel Gil Peyrot <linkmauve@linkmauve.fr>
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

#![deny(bare_trait_objects)]

use crate::error::mk_bad_request_stanza;
use crate::interpret_message_type::{infer_incoming_message_context, InferredMessageContext};
use crate::parsers::delay::Delay;
use crate::parsers::message_correct::Replace;
use crate::parsers::retraction::Retract;
use crate::parsers::stanza_id::StanzaId;
use chrono::{DateTime as ChronoDateTime, FixedOffset, Utc};
use futures::stream::StreamExt;
use reqwest::{
    header::HeaderMap as ReqwestHeaderMap, Body as ReqwestBody, Client as ReqwestClient,
};
use std::path::{Path, PathBuf};
use std::sync::{Arc, RwLock};
use tokio::fs::File;
use tokio_util::codec::{BytesCodec, FramedRead};
pub use tokio_xmpp::parsers;
use tokio_xmpp::parsers::{
    bookmarks, bookmarks2,
    caps::{compute_disco, hash_caps, Caps},
    disco::{DiscoInfoQuery, DiscoInfoResult, Feature, Identity},
    hashes::Algo,
    http_upload::{Header as HttpUploadHeader, SlotRequest, SlotResult},
    iq::{Iq, IqType},
    message::{Body, Message, MessageType},
    muc::{
        user::{MucUser, Status},
        Muc,
    },
    ns,
    presence::{Presence, Type as PresenceType},
    private::Query as PrivateXMLQuery,
    pubsub::pubsub::{Items, PubSub},
    roster::{Item as RosterItem, Roster},
    stanza_error::{DefinedCondition, ErrorType, StanzaError},
    vcard::{Photo, VCard},
    Error as ParsersError,
};
use tokio_xmpp::{AsyncClient as TokioXmppClient, Event as TokioXmppEvent};
pub use tokio_xmpp::{BareJid, Element, FullJid, Jid};

#[macro_use]
extern crate log;

mod error;
mod interpret_message_type;
mod pubsub;

pub type Error = tokio_xmpp::Error;

#[derive(Debug)]
pub enum ClientType {
    Bot,
    Pc,
}

impl Default for ClientType {
    fn default() -> Self {
        ClientType::Bot
    }
}

impl ToString for ClientType {
    fn to_string(&self) -> String {
        String::from(match self {
            ClientType::Bot => "bot",
            ClientType::Pc => "pc",
        })
    }
}

#[derive(PartialEq)]
pub enum ClientFeature {
    #[cfg(feature = "avatars")]
    Avatars,
    ContactList,
    JoinRooms,
}

pub type Id = String;
pub type RoomNick = String;

/// Whether a given piece of information was explicitly
/// reported in a stanza, or inferred from context.
#[derive(Debug, Clone)]
pub enum ReportedOrInferred {
    /// The information was explicitly reported in a stanza.
    Reported,
    /// The information was inferred from context.
    Inferred,
}

/// The reported sending time of a stanza, as per XEP-0203.
///
/// Warning: This can be spoofed quite easily, so should not be blindly trusted in any case
/// where security is important, such as when ordering messages to prevent gaslighting and
/// similar malicious use.
///
/// For security implications, see: https://xmpp.org/extensions/xep-0203.html#security
#[derive(Debug, Clone)]
pub struct SendTimeStamp {
    /// The timestamp itself.
    pub stamp: ChronoDateTime<FixedOffset>,
    /// Whether the timestamp was reported from a <delay> element in the stanza,
    /// or inferred from the absence thereof.
    pub source: ReportedOrInferred,
}

#[derive(Debug)]
pub enum Event {
    Online,
    Disconnected(Error),
    ContactAdded(RosterItem),
    ContactRemoved(RosterItem),
    ContactChanged(RosterItem),
    #[cfg(feature = "avatars")]
    AvatarRetrieved(Jid, String),
    /// A chat message was received, either because it was just sent, as a message carbon, or from an archive.
    ///
    /// - The Id is a unique identifier for this message.
    /// - The BareJid is the sender's JID.
    /// - The Body is the message body.
    /// - The timestamp is the time at which the message was reportedly originally sent.
    ///
    /// Note that this sending time may be different from the time at which the message was received by the client.
    /// When using this timestamp, security considerations should be taken into account, as noted here:
    /// https://xmpp.org/extensions/xep-0203.html#security
    ChatMessage(Option<Id>, BareJid, Body, SendTimeStamp),
    /// A photo is available for a given Jid, with the given hash.
    ///
    /// If the client is interested in the photo, it should call `retrieve_avatar` with the Jid.
    PhotoAvailable(Jid, String),
    /// A photo has been retrieved for a given Jid. (TODO: Unify with AvatarRetrieved?)
    PhotoRetrieved(Jid, Photo),
    JoinRoom(BareJid, bookmarks2::Conference),
    LeaveRoom(BareJid),
    LeaveAllRooms,
    RoomJoined(BareJid),
    RoomLeft(BareJid),
    /// A message received from a MUC, containing the message ID, the room's BareJid.
    ///
    /// This event may be from a message that was recently sent, from a message carbon, or from an archive.
    RoomMessage {
        /// A unique ID for this message as used in the MUC
        message_id: Option<Id>,
        /// Original Id as assigned by the sender (This is the Id returned by send_message)
        origin_id: Option<Id>,
        /// The JID of the MUC
        muc_jid: BareJid,
        /// The nickname of the sender within the MUC
        muc_member: RoomNick,
        /// The message body
        body: Body,
        /// The timestamp is the time at which the message was reportedly originally sent.
        ///   Note that this sending time may be different from the time at which the message was received by the client.
        ///   When using this timestamp, security considerations should be taken into account, as noted here:
        ///   https://xmpp.org/extensions/xep-0203.html#security
        send_timestamp: SendTimeStamp,
    },
    /// A message received from a MUC, containing the message ID, the room's BareJid.
    ///
    /// This event may be from a message that was recently sent, from a message carbon, or from an archive.
    RoomPrivateMessage {
        /// A unique ID for this message as used in the MUC
        message_id: Option<Id>,
        /// Original Id as assigned by the sender (This is the Id returned by send_message)
        origin_id: Option<Id>,
        /// The JID of the MUC
        muc_jid: BareJid,
        /// The nickname of the sender within the MUC
        muc_member: RoomNick,
        /// The message body
        body: Body,
        /// The timestamp is the time at which the message was reportedly originally sent.
        ///   Note that this sending time may be different from the time at which the message was received by the client.
        ///   When using this timestamp, security considerations should be taken into account, as noted here:
        ///   https://xmpp.org/extensions/xep-0203.html#security
        send_timestamp: SendTimeStamp,
    },
    /// A service message was received. (TODO: Find out if this is always from a MUC or can also be from a private chat.)
    ///
    /// This event may be from a message that was recently sent, from a message carbon, or from an archive.
    ///
    /// - The Id is a unique identifier for this message.
    /// - The BareJid is the MUC's JID.
    /// - The Body is the message body.
    /// - The timestamp is the time at which the message was originally sent.
    ///
    ///   Note that this sending time may be different from the time at which the message was received by the client.
    ///   When using this timestamp, security considerations should be taken into account, as noted here:
    ///   https://xmpp.org/extensions/xep-0203.html#security
    /// A message in a room was retracted.
    /// - The Id is the ID of the message that was retracted.
    /// - The BareJid is the JID of the contact who retracted the message.
    /// TODO: What about tombstones? See https://xmpp.org/extensions/xep-0424.html#tombstones
    ChatMessageRetracted(Id, BareJid),
    /// A message in a MUC was retracted.
    /// - The Id is the ID of the message that was retracted.
    /// - The BareJid is the JID of the room where the message was retracted.
    /// - The RoomNick is the nickname of the user who retracted the message.
    /// TODO: What about tombstones? See https://xmpp.org/extensions/xep-0424.html#tombstones
    RoomMessageRetracted(Id, BareJid, RoomNick),
    /// A private message in a MUC was retracted.
    /// - The Id is the ID of the message that was retracted.
    /// - The BareJid is the JID of the room where the message was retracted.
    /// - The RoomNick is the nickname of the user who retracted the message.
    /// TODO: What about tombstones? See https://xmpp.org/extensions/xep-0424.html#tombstones
    RoomPrivateMessageRetracted(Id, BareJid, RoomNick),
    /// A message in a room was corrected.
    /// - The Id is the ID of the message that was corrected.
    /// - The BareJid is the JID of the contact who corrected the message.
    /// - The Body is the new message body.
    /// - The timestamp is the time at which the correction was originally sent.
    ChatMessageCorrected(Id, BareJid, Body, SendTimeStamp),
    /// A message in a MUC was corrected.
    /// - The Id is the ID of the message that was corrected.
    /// - The BareJid is the JID of the room where the message was corrected.
    /// - The RoomNick is the nickname of the user who corrected the message.
    ///   (TODO: Check that this matches the sender of the original message.)
    RoomMessageCorrected(Id, BareJid, RoomNick, Body, SendTimeStamp),
    /// A private message in a MUC was corrected.
    /// - The Id is the ID of the message that was corrected.
    /// - The BareJid is the JID of the room where the message was corrected.
    /// - The RoomNick is the nickname of the user who corrected the message.
    /// - The Body is the new message body.
    /// - The timestamp is the time at which the correction was originally sent.
    RoomPrivateMessageCorrected(Id, BareJid, RoomNick, Body, SendTimeStamp),
    ServiceMessage(Option<Id>, BareJid, Body, SendTimeStamp),
    /// The subject of a MUC was received.
    /// - The BareJid is the MUC's JID.
    /// - The RoomNick is the nickname of the MUCmember who set the subject.
    /// - The Subject is the new subject.
    RoomSubject(BareJid, Option<RoomNick>, String),
    HttpUploadedFile(String),
}

pub struct ClientBuilder<'a> {
    jid: BareJid,
    password: &'a str,
    website: String,
    default_nick: String,
    lang: Vec<String>,
    disco: (ClientType, String),
    features: Vec<ClientFeature>,
    resource: Option<String>,
}

impl ClientBuilder<'_> {
    pub fn new<'a>(jid: BareJid, password: &'a str) -> ClientBuilder<'a> {
        ClientBuilder {
            jid,
            password,
            website: String::from("https://gitlab.com/xmpp-rs/tokio-xmpp"),
            default_nick: String::from("xmpp-rs"),
            lang: vec![String::from("en")],
            disco: (ClientType::default(), String::from("tokio-xmpp")),
            features: vec![],
            resource: None,
        }
    }

    /// Optionally set a resource associated to this device on the client
    pub fn set_resource(mut self, resource: &str) -> Self {
        self.resource = Some(resource.to_string());
        self
    }

    pub fn set_client(mut self, type_: ClientType, name: &str) -> Self {
        self.disco = (type_, String::from(name));
        self
    }

    pub fn set_website(mut self, url: &str) -> Self {
        self.website = String::from(url);
        self
    }

    pub fn set_default_nick(mut self, nick: &str) -> Self {
        self.default_nick = String::from(nick);
        self
    }

    pub fn set_lang(mut self, lang: Vec<String>) -> Self {
        self.lang = lang;
        self
    }

    pub fn enable_feature(mut self, feature: ClientFeature) -> Self {
        self.features.push(feature);
        self
    }

    fn make_disco(&self) -> DiscoInfoResult {
        let identities = vec![Identity::new(
            "client",
            self.disco.0.to_string(),
            "en",
            self.disco.1.to_string(),
        )];
        let mut features = vec![Feature::new(ns::DISCO_INFO)];
        #[cfg(feature = "avatars")]
        {
            if self.features.contains(&ClientFeature::Avatars) {
                features.push(Feature::new(format!("{}+notify", ns::AVATAR_METADATA)));
            }
        }
        if self.features.contains(&ClientFeature::JoinRooms) {
            features.push(Feature::new(format!("{}+notify", ns::BOOKMARKS2)));
        }
        // TODO feature flag.
        features.push(Feature::new(ns::MESSAGE_CORRECT));

        DiscoInfoResult {
            node: None,
            identities,
            features,
            extensions: vec![],
        }
    }

    pub fn build(self) -> Agent {
        let jid: Jid = if let Some(resource) = &self.resource {
            self.jid.with_resource_str(resource).unwrap().into()
        } else {
            self.jid.clone().into()
        };

        let client = TokioXmppClient::new(jid, self.password);
        self.build_impl(client)
    }

    // This function is meant to be used for testing build
    pub(crate) fn build_impl(self, client: TokioXmppClient) -> Agent {
        let disco = self.make_disco();
        let node = self.website;

        Agent {
            client,
            default_nick: Arc::new(RwLock::new(self.default_nick)),
            lang: Arc::new(self.lang),
            disco,
            node,
            uploads: Vec::new(),
            awaiting_disco_bookmarks_type: false,
        }
    }
}

pub struct Agent {
    client: TokioXmppClient,
    default_nick: Arc<RwLock<String>>,
    lang: Arc<Vec<String>>,
    disco: DiscoInfoResult,
    node: String,
    uploads: Vec<(String, Jid, PathBuf)>,
    awaiting_disco_bookmarks_type: bool,
}

impl Agent {
    pub async fn disconnect(&mut self) -> Result<(), Error> {
        self.client.send_end().await
    }

    pub async fn join_room(
        &mut self,
        room: BareJid,
        nick: Option<String>,
        password: Option<String>,
        lang: &str,
        status: &str,
    ) {
        let mut muc = Muc::new();
        if let Some(password) = password {
            muc = muc.with_password(password);
        }

        let nick = nick.unwrap_or_else(|| self.default_nick.read().unwrap().clone());
        let room_jid = room.with_resource_str(&nick).unwrap();
        let mut presence = Presence::new(PresenceType::None).with_to(room_jid);
        presence.add_payload(muc);
        presence.set_status(String::from(lang), String::from(status));
        let _ = self.client.send_stanza(presence.into()).await;
    }

    /// Send a "leave room" request to the server (specifically, an "unavailable" presence stanza).
    ///
    /// The returned future will resolve when the request has been sent,
    /// not when the room has actually been left.
    ///
    /// If successful, a `RoomLeft` event should be received later as a confirmation.
    ///
    /// See: https://xmpp.org/extensions/xep-0045.html#exit
    ///
    /// Note that this method does NOT remove the room from the auto-join list; the latter
    /// is more a list of bookmarks that the account knows about and that have a flag set
    /// to indicate that they should be joined automatically after connecting (see the JoinRoom event).
    ///
    /// Regarding the latter, see the these minutes about auto-join behavior:
    /// https://docs.modernxmpp.org/meetings/2019-01-brussels/#bookmarks
    ///
    /// # Arguments
    ///
    /// * `room_jid`: The JID of the room to leave.
    /// * `nickname`: The nickname to use in the room.
    /// * `lang`: The language of the status message.
    /// * `status`: The status message to send.
    pub async fn leave_room(
        &mut self,
        room_jid: BareJid,
        nickname: RoomNick,
        lang: impl Into<String>,
        status: impl Into<String>,
    ) {
        // XEP-0045 specifies that, to leave a room, the client must send a presence stanza
        // with type="unavailable".
        let mut presence = Presence::new(PresenceType::Unavailable).with_to(
            room_jid
                .with_resource_str(nickname.as_str())
                .expect("Invalid room JID after adding resource part."),
        );

        // Optionally, the client may include a status message in the presence stanza.
        // TODO: Should this be optional? The XEP says "MAY", but the method signature requires the arguments.
        // XEP-0045: "The occupant MAY include normal <status/> information in the unavailable presence stanzas"
        presence.set_status(lang, status);

        // Send the presence stanza.
        if let Err(e) = self.client.send_stanza(presence.into()).await {
            // Report any errors to the log.
            error!("Failed to send leave room presence: {}", e);
        }
    }

    /// Send a message to a given recipient.
    ///
    /// Returns the generated message ID, or an error if the message could not be sent.
    ///
    /// # Arguments
    ///
    /// * `recipient`: The JID of the recipient.
    /// * `type_`: The type of message to send.
    ///            For a message to a MUC room, this should be `MessageType::Groupchat`.
    ///            For a private message to a MUC room member or regular contact,
    ///            this should be `MessageType::Chat`.
    /// * `lang`:  The language of the message. (See IETF RFC 5646)
    /// * `text`:  The text of the message.
    pub async fn create_room_bookmark(
        &mut self,
        room: &BareJid,
        auto_join: bool,
        nickname: Option<RoomNick>,
        password: Option<String>,
    ) {
        let iq = pubsub::mk_create_bookmark_iq(room, auto_join, nickname, password);

        _ = self.client.send_stanza(iq.into()).await;
    }

    pub async fn send_message(
        &mut self,
        recipient: Jid,
        type_: MessageType,
        lang: &str,
        text: &str,
    ) -> Result<Id, tokio_xmpp::Error> {
        // Create a new message stanza.
        let mut message = Message::new(Some(recipient));
        message.type_ = type_;

        // Add the message body.
        message
            .bodies
            .insert(String::from(lang), Body(String::from(text)));

        // Send the message.
        self.client.send_stanza(message.into()).await
    }

    /// Request a message retraction according to XEP-0424
    ///
    /// # Arguments
    /// * `recipient`: The JID of the recipient of the message to retract.
    /// * `to_retract_id`: The ID of the message to retract.
    ///
    /// # Panics
    /// This method panics if the stanza could not be sent.
    ///
    /// The future completes once the retraction request has been sent.
    pub async fn request_retraction(
        &mut self,
        recipient: BareJid,
        to_retract_id: String,
        type_: MessageType,
    ) {
        // Create a message stanza with a Retract payload.
        let mut message = Message::new(Some(recipient.into()))
            .with_payload(parsers::retraction::Retract { id: to_retract_id });
        // TODO: Should include a <fallback/> element as per XEP-0424.

        // Set the message type.
        message.type_ = type_;

        // Send the message.
        self.client
            .send_stanza(message.into())
            .await
            .expect("Failed to send retraction request");
    }

    /// Send a message correction according to XEP-0308.
    ///
    /// Note: XEP-0308 specifies that the *last* message can be corrected,
    /// and leaves it ambiguous what to do if the id is of an earlier message.
    ///
    /// Results may vary if you try to correct a message that is not the last one,
    /// but this restriction is not enforced in xmpp-rs.
    ///
    /// # Arguments
    ///
    /// * `recipient`: The JID of the recipient of the message to correct.
    /// * `to_retract_id`: The ID of the message to correct.
    /// * `type_`: The type of message to send.
    /// * `lang`: The language of the message. (See IETF RFC 5646)
    /// * `text`: The text of the message.
    pub async fn request_correction(
        &mut self,
        recipient: BareJid,
        to_retract_id: String,
        type_: MessageType,
        lang: &str,
        text: &str,
    ) {
        // Create a message stanza with a Retract payload.
        let mut message = Message::new(Some(recipient.into()))
            .with_payload(parsers::message_correct::Replace { id: to_retract_id })
            .with_body(lang.to_string(), text.to_string());

        // Set the message type.
        message.type_ = type_;

        // Send the message.
        self.client
            .send_stanza(message.into())
            .await
            .expect("Failed to send correction request");
    }

    /// Send a private message to a member of a MUC room.
    ///
    /// Returns the generated message ID.
    ///
    /// # Arguments
    ///
    /// * `room`: The JID of the room.
    /// * `recipient`: The nickname of the recipient.
    /// * `lang`: The language of the message. (See IETF RFC 5646)
    /// * `text`: The text of the message.
    pub async fn send_room_private_message(
        &mut self,
        room: BareJid,
        recipient: RoomNick,
        lang: &str,
        text: &str,
    ) -> Result<Id, tokio_xmpp::Error> {
        // Create a new message stanza.
        let recipient: Jid = room.with_resource_str(&recipient).unwrap().into();

        // Create a new message stanza.
        let mut message = Message::new(recipient)
            // Put in a MUC user payload to distinguish this from a regular direct message.
            .with_payload(MucUser::new());

        // Private MUC essages are of type "chat".
        message.type_ = MessageType::Chat;

        // Add the message body.
        message
            .bodies
            .insert(String::from(lang), Body(String::from(text)));

        // Send the message.
        self.client.send_stanza(message.into()).await
    }

    /// Get the bound jid of the client.
    ///
    /// If the client is not connected, this will be None.
    pub fn bound_jid(&self) -> Option<&Jid> {
        self.client.bound_jid()
    }

    /// Get the default nick.
    pub fn default_nick(&self) -> String {
        self.default_nick.read().unwrap().clone()
    }

    fn make_initial_presence(disco: &DiscoInfoResult, node: &str) -> Presence {
        let caps_data = compute_disco(disco);
        let hash = hash_caps(&caps_data, Algo::Sha_1).unwrap();
        let caps = Caps::new(node, hash);

        let mut presence = Presence::new(PresenceType::None);
        presence.add_payload(caps);
        presence
    }

    /// Handle an <iq> stanza received from the server
    /// Returns a Vec of higher-level events.
    async fn handle_iq(&mut self, iq: Iq) -> Vec<Event> {
        // Allocate a vec of Events.
        let mut events = vec![];

        // Extract the JID of the sender, assuming the client's bound jid if absent.
        let from = iq
            .from
            .clone()
            .unwrap_or_else(|| self.client.bound_jid().unwrap().clone());

        // Handle the iq according to its type.
        if let IqType::Get(payload) = iq.payload {
            // The server wants information from us.

            if payload.is("query", ns::DISCO_INFO) {
                // The server wants our discovery information.

                // Try to parse the discovery query.
                let query = DiscoInfoQuery::try_from(payload);

                match query {
                    Ok(query) => {
                        // Parse succeeded; answer the query.
                        let mut disco_info = self.disco.clone();

                        // Declare that the node we're answering for is the one the server asked about.
                        disco_info.node = query.node;

                        // Construct a iq result with the disco info.
                        let iq = Iq::from_result(iq.id, Some(disco_info))
                            .with_to(iq.from.unwrap()) // TODO: The server can crash XMPP-RS if the `from` is not set?
                            .into();

                        // Send the stanza.
                        if let Err(e) = self.client.send_stanza(iq).await {
                            error!("Error while sending Iq stanza: {}", e);
                        }
                    }

                    Err(err) => {
                        // Parse failure; the server possibly set a malformed stanza.

                        // Construct a StanzaError that reports a bad request.
                        let error = StanzaError::new(
                            ErrorType::Modify,
                            DefinedCondition::BadRequest,
                            "en",
                            &format!("{}", err),
                        );

                        // Construct a response iq stanza.
                        let iq = Iq::from_error(iq.id, error)
                            .with_to(iq.from.unwrap())
                            .into();

                        // Send the result stanza.
                        if let Err(e) = self.client.send_stanza(iq).await {
                            error!("Error while sending Iq error (bad request) stanza: {}", e);
                        }
                    }
                }
            } else {
                // If we get here, we do not know how to answer this query.

                // We MUST answer unhandled get iqs with a service-unavailable error.
                let error = StanzaError::new(
                    ErrorType::Cancel,
                    DefinedCondition::ServiceUnavailable,
                    "en",
                    "No handler defined for this kind of iq.",
                );

                // Construct the Error stanza.
                let iq = Iq::from_error(iq.id, error)
                    .with_to(iq.from.unwrap())
                    .into();

                // Send it and report errors.
                if let Err(e) = self.client.send_stanza(iq).await {
                    error!(
                        "Error while sending Iq error (service unavailable) stanza: {}",
                        e
                    );
                }
            }
        } else if let IqType::Result(Some(payload)) = iq.payload {
            // TODO: move private iqs like this one somewhere else, for
            // security reasons.
            if payload.is("query", ns::ROSTER) {
                // Received RFC6121 roster (contact list)
                // Make sure it's from the account we're using (no from, or from = self's BareJid)
                if iq.from.is_none() || self.client.bound_jid().unwrap() == &iq.from.unwrap() {
                    // This is a response to a roster query.  (TODO: Why are we checking iq.from?)

                    // Parse the roster.
                    let roster = Roster::try_from(payload).unwrap(); // TODO: The server can crash xmpp-rs by sending a malformed roster!

                    // Emit an event for every roster item for the user to interpret.
                    for item in roster.items.into_iter() {
                        events.push(Event::ContactAdded(item));
                    }
                }
            } else if payload.is("pubsub", ns::PUBSUB) {
                // This is a pubsub iq result; handle it separately.
                let new_events = pubsub::handle_iq_result(&from, payload);
                events.extend(new_events);
            } else if payload.is("slot", ns::HTTP_UPLOAD) {
                // This is an iq result related to an HTTP upload.
                let new_events = handle_upload_result(&from, iq.id, payload, self).await;
                events.extend(new_events);
            } else if payload.is("query", ns::PRIVATE) {
                match PrivateXMLQuery::try_from(payload) {
                    Ok(query) => {
                        for conf in query.storage.conferences {
                            let (jid, room) = conf.into_bookmarks2();
                            events.push(Event::JoinRoom(jid, room));
                        }
                    }
                    Err(e) => {
                        panic!("Wrong XEP-0048 v1.0 Bookmark format: {}", e);
                    }
                }
            } else if payload.is("query", ns::DISCO_INFO) {
                self.handle_disco_info_result_payload(payload, from).await;
            } else if payload.is("vCard", "vcard-temp") {
                events.extend(Self::handle_vcard(from, &payload).await);
            } else {
                // We received an iq result of a type we can't handle.
                // We are getting responses we don't understand for a request we presumably made?
                error!("Unhandled result stanza payload: {:?}", payload);
            }
        } else if let IqType::Set(_) = iq.payload {
            // The server wants to set a value on our side; we do not currently support this.

            // We MUST answer unhandled set iqs with a service-unavailable error.
            let error = StanzaError::new(
                ErrorType::Cancel,
                DefinedCondition::ServiceUnavailable,
                "en",
                "No handler defined for this kind of iq.",
            );

            let iq = Iq::from_error(iq.id, error)
                .with_to(iq.from.unwrap()) // TODO: Server can crash us by omitting a `from` attribute.
                .into();

            if let Err(e) = self.client.send_stanza(iq).await {
                error!(
                    "Error while sending Iq error (service unavailable) stanza: {}",
                    e
                );
            }
        }

        // Return all events generated as a result of processing this stanza.
        events
    }

    /// Handle a <vCard> element received from the server.
    async fn handle_vcard(from: Jid, payload: &Element) -> impl IntoIterator<Item = Event> {
        let mut events = vec![];

        let vcard = match VCard::try_from(payload.clone()) {
            Ok(vcard) => vcard,
            Err(e) => {
                error!("Error while decoding vCard: {:?}", e);
                return events;
            }
        };

        // Convert the optional photo into an Event vec.
        if let Some(photo) = vcard.photo {
            events.push(Event::PhotoRetrieved(from, photo));
        }

        // Return the events.
        events
    }

    // This method is a workaround due to prosody bug https://issues.prosody.im/1664
    // FIXME: To be removed in the future
    // The server doesn't return disco#info feature when querying the account
    // so we add it manually because we know it's true
    async fn handle_disco_info_result_payload(&mut self, payload: Element, from: Jid) {
        match DiscoInfoResult::try_from(payload.clone()) {
            Ok(disco) => {
                self.handle_disco_info_result(disco, from).await;
            }
            Err(e) => match e {
                ParsersError::ParseError(reason) => {
                    if reason == "disco#info feature not present in disco#info." {
                        let mut payload = payload.clone();
                        let disco_feature =
                            Feature::new("http://jabber.org/protocol/disco#info").into();
                        payload.append_child(disco_feature);
                        match DiscoInfoResult::try_from(payload) {
                            Ok(disco) => {
                                self.handle_disco_info_result(disco, from).await;
                            }
                            Err(e) => {
                                panic!("Wrong disco#info format after workaround: {}", e)
                            }
                        }
                    } else {
                        panic!(
                            "Wrong disco#info format (reason cannot be worked around): {}",
                            e
                        )
                    }
                }
                _ => panic!("Wrong disco#info format: {}", e),
            },
        }
    }

    async fn handle_disco_info_result(&mut self, disco: DiscoInfoResult, from: Jid) {
        // Safe unwrap because no DISCO is received when we are not online
        if from == self.client.bound_jid().unwrap().to_bare() && self.awaiting_disco_bookmarks_type
        {
            info!("Received disco info about bookmarks type");
            // Trigger bookmarks query
            // TODO: only send this when the JoinRooms feature is enabled.
            self.awaiting_disco_bookmarks_type = false;
            let mut perform_bookmarks2 = false;
            info!("{:#?}", disco.features);
            for feature in disco.features {
                if feature.var == "urn:xmpp:bookmarks:1#compat" {
                    perform_bookmarks2 = true;
                }
            }

            if perform_bookmarks2 {
                // XEP-0402 bookmarks (modern)
                let iq =
                    Iq::from_get("bookmarks", PubSub::Items(Items::new(ns::BOOKMARKS2))).into();
                let _ = self.client.send_stanza(iq).await;
            } else {
                // XEP-0048 v1.0 bookmarks (legacy)
                let iq = Iq::from_get(
                    "bookmarks-legacy",
                    PrivateXMLQuery {
                        storage: bookmarks::Storage::new(),
                    },
                )
                .into();
                let _ = self.client.send_stanza(iq).await;
            }
        } else {
            unimplemented!("Ignored disco#info response from {}", from);
        }
    }

    async fn handle_message(&mut self, message: Message) -> Vec<Event> {
        // Allocate an empty vector to store the events.
        let mut events = vec![];

        // Extract the JID of the sender (i.e. the one who sent the message).
        let from = match message.from.clone() {
            Some(from) => from,
            None => {
                // If the message has no 'from' attribute, then it is malformed.
                // Send a <bad-request/> error to the sender and return an empty list of events.
                self.client
                    .send_stanza(
                        mk_bad_request_stanza(
                            &message,
                            "Message has no 'from' attribute.".to_string(),
                        )
                        .into(),
                    )
                    .await
                    .unwrap();
                return events;
            }
        };

        // Extract the list of languages that the client prefers.
        let langs: Vec<&str> = self.lang.iter().map(String::as_str).collect();

        let timestamp = Self::deduce_message_timestamp(&message);

        // Check if this has a <retract/> payload.
        if let Some(retract) = message
            .payloads
            .iter()
            .find(|p| p.is("retract", ns::RETRACTIONS))
        {
            match Retract::try_from(retract.clone()) {
                Ok(Retract { id }) => {
                    // TODO: Because xmpp-rs does not retain who sent what message, we cannot
                    // prevent emitting events when someone tries to retract a message that
                    // is not theirs.
                    match infer_incoming_message_context(&message)
                        .expect("Failed to infer message context")
                    {
                        // Direct message => emit a ChatMessageRetracted event.
                        InferredMessageContext::Direct { .. } => {
                            events.push(Event::ChatMessageRetracted(id, from.to_bare()));
                        }
                        // MUC message => emit a RoomMessageRetracted event.
                        InferredMessageContext::MucPublic { in_room } => {
                            events.push(Event::RoomMessageRetracted(
                                id,
                                in_room.clone(),
                                from.resource_str().map(String::from).unwrap(),
                            ));
                        }
                        // MUC private message => emit a RoomPrivateMessageRetracted event.
                        InferredMessageContext::MucPrivate {
                            in_room,
                            with_member: _,
                        } => {
                            events.push(Event::RoomMessageRetracted(
                                id,
                                in_room.clone(),
                                from.resource_str().map(String::from).unwrap(),
                            ));
                        }
                    }
                }
                Err(e) => {
                    error!("Failed to parse retraction: {}", &e);
                    // Send a <bad-request/> error to the sender.
                    self.client
                        .send_stanza(mk_bad_request_stanza(&message, e.to_string()).into())
                        .await
                        .unwrap();

                    return events;
                }
            }

            // We're done processing this message.
            return events;
        }

        if let Some(replace) = message
            .payloads
            .iter()
            .find(|p| p.is("replace", ns::MESSAGE_CORRECT))
        {
            match Replace::try_from(replace.clone()) {
                Ok(Replace { id }) => {
                    let body = message.get_best_body(langs.clone()).unwrap().1;

                    match infer_incoming_message_context(&message)
                        .expect("Failed to infer message context")
                    {
                        InferredMessageContext::Direct { .. } => {
                            events.push(Event::ChatMessageCorrected(
                                id,
                                from.to_bare(),
                                body.clone(),
                                timestamp,
                            ));
                        }
                        InferredMessageContext::MucPrivate { .. } => {
                            events.push(Event::RoomMessageCorrected(
                                id,
                                from.to_bare(),
                                from.resource_str()
                                    .expect("A MUC private message without a resource")
                                    .to_string(), // TODO Can service messages be corrected?
                                body.clone(),
                                timestamp,
                            ));
                        }
                        InferredMessageContext::MucPublic { .. } => {
                            events.push(Event::RoomMessageCorrected(
                                id,
                                from.to_bare(),
                                from.resource_str()
                                    .expect("A MUC public message without a resource")
                                    .to_string(), // TODO Can service messages be corrected?
                                body.clone(),
                                timestamp,
                            ));
                        }
                    }
                }
                Err(e) => {
                    error!("Failed to parse correction: {}", &e);
                    // Send a <bad-request/> error to the sender.
                    self.client
                        .send_stanza(mk_bad_request_stanza(&message, e.to_string()).into())
                        .await
                        .unwrap();

                    return events;
                }
            }

            // We're done processing this message.
            return events;
        }

        // Extract the message body, if any, preferring the best language.
        if let Some((_lang, body)) = message.get_best_body(langs.clone()) {
            // Infer the context of the message (MUC, private, direct, etc.).
            match infer_incoming_message_context(&message) {
                // Direct message => emit a ChatMessage event.
                Ok(InferredMessageContext::Direct { .. }) => events.push(Event::ChatMessage(
                    message.id.clone(),
                    from.to_bare(),
                    body.clone(),
                    timestamp,
                )),
                // MUC message => emit a RoomMessage or ServiceMessage event.
                Ok(InferredMessageContext::MucPublic { in_room }) => {
                    match message
                        .from
                        .as_ref()
                        .expect("Message without from attribute")
                    {
                        // If the message is from a full JID, then it's a RoomMessage.
                        Jid::Full(full) => {
                            // Find a <stanza-id> payload and use that, or the message id as a fallback.
                            let message_id = message
                                .payloads
                                .iter()
                                .find(|p| p.is("stanza-id", ns::SID))
                                .map(|p| {
                                    StanzaId::try_from(p.clone())
                                        .expect("Failed to parse stanza-id")
                                        .id
                                })
                                .unwrap_or_else(|| message.id.clone().expect("Message without id"));

                            events.push(Event::RoomMessage {
                                message_id: Some(message_id),
                                origin_id: message.id.clone(),
                                muc_jid: in_room.clone(),
                                muc_member: full.resource_str().to_owned(),
                                body: body.clone(),
                                send_timestamp: timestamp,
                            })
                        }
                        // Otherwise, the room itself sent the message, so it's a ServiceMessage.
                        Jid::Bare(room_jid) => events.push(Event::ServiceMessage(
                            message.id.clone(),
                            room_jid.clone(),
                            body.clone(),
                            timestamp,
                        )),
                    }
                }
                // MUC private message => emit a RoomPrivateMessage event.
                Ok(InferredMessageContext::MucPrivate {
                    in_room,
                    with_member,
                }) => {
                    // Find a <stanza-id> payload and use that, or the message id as a fallback.
                    let message_id = message
                        .payloads
                        .iter()
                        .find(|p| p.is("stanza-id", ns::SID))
                        .map(|p| {
                            StanzaId::try_from(p.clone())
                                .expect("Failed to parse stanza-id")
                                .id
                        })
                        .unwrap_or_else(|| message.id.clone().expect("Message without id"));

                    events.push(Event::RoomPrivateMessage {
                        origin_id: message.id.clone(),
                        message_id: Some(message_id),
                        muc_jid: in_room.clone(),
                        muc_member: with_member.clone(),
                        body: body.clone(),
                        send_timestamp: timestamp,
                    })
                }
                // Unknown context (with known error) => send a <bad-request/> error to the sender.
                Err(e) => {
                    error!("Failed to infer message type: {}", &e);

                    // Send a <bad-request/> error to the sender.
                    self.client
                        .send_stanza(mk_bad_request_stanza(&message, e.to_string()).into())
                        .await
                        .unwrap();
                }
            }
        }

        // Search through the payloads for additional information.

        // If the messege contains a subject, emit a RoomSubject event as defined in https://xmpp.org/extensions/xep-0045.html#enter-subject
        if let Some((_lang, subject)) = message.get_best_subject(langs) {
            // AFAIK, the subject is only valid in groupchat messages since it's defined in XEP, so check that.
            if message.type_ != MessageType::Groupchat {
                panic!("Received subject in non-groupchat message.");
            }

            // Emit the event.
            events.push(Event::RoomSubject(
                from.to_bare(),
                from.resource_str().map(String::from),
                subject.0.clone(),
            ));
        }

        for child in message.payloads {
            if child.is("event", ns::PUBSUB_EVENT) {
                let new_events = pubsub::handle_event(&from, child, self).await;
                events.extend(new_events);
            }
        }

        events
    }

    /// Given a Message, deduce the timestamp of the message according to [XEP-0203](https://xmpp.org/extensions/xep-0203.html#protocol)
    ///
    /// Specifically, this method will look for a <delay/> element in the message's payloads.
    ///
    /// If found, it will return the timestamp contained in the delay element,
    /// otherwise it will assume no significant delay has occurred and return the current time.
    fn deduce_message_timestamp(message: &Message) -> SendTimeStamp {
        // Find a <delay> element in the payloads.
        if let Some(delay) = message.payloads.iter().find(|p| p.is("delay", ns::DELAY)) {
            // If found, try to parse.
            if let Ok(delay) = Delay::try_from(delay.clone()) {
                // Successfully parsed; return the timestamp.
                return SendTimeStamp {
                    stamp: delay.stamp.0,
                    source: ReportedOrInferred::Reported,
                };
            } else {
                // Failed to parse; the server sent us a malformed delay element.
                // TODO: We should probably instead return an error stanza to the sender.
                panic!("Failed to parse delay element: {}", String::from(delay));
            }
        } else {
            // No delay element was found; return the current time.
            return SendTimeStamp {
                stamp: ChronoDateTime::from(Utc::now()),
                source: ReportedOrInferred::Inferred,
            };
        }
    }

    /// Translate a `Presence` stanza into a list of higher-level `Event`s.
    async fn handle_presence(&mut self, presence: Presence) -> Vec<Event> {
        // Allocate an empty vector to store the events.
        let mut events = vec![];

        for payload in presence.payloads {
            // It's a vcard update.
            if payload.is("x", "vcard-temp:x:update") {
                // Does it contain a photo? (https://xmpp.org/extensions/xep-0153.html#example-3)
                if let Some(photo) = payload.get_child("photo", "vcard-temp:x:update") {
                    // Tell the client that a photo is available for the sender; it's a sha1 hash.
                    // About the string being empty: https://xmpp.org/extensions/xep-0153.html#example-7`
                    // i.e. There is explicitly no image available; the client might want to delete the image it has, if any.
                    events.push(Event::PhotoAvailable(
                        presence.from.clone().unwrap(),
                        photo.text().to_owned(),
                    ));
                }
            } else if let Some(muc) = MucUser::try_from(payload.clone()).ok() {
                // It's a MUC user status.

                // Extract the JID of the sender (i.e. the one whose presence is being sent).
                let from = presence.from.clone().unwrap().to_bare();

                // If a MUC user status was found, search through the statuses for a self-presence.
                if muc.status.iter().any(|s| *s == Status::SelfPresence) {
                    // If a self-presence was found, then the stanza is about the client's own presence.
                    match presence.type_ {
                        PresenceType::None => {
                            // According to https://xmpp.org/extensions/xep-0045.html#enter-pres, no type should be seen as "available".
                            events.push(Event::RoomJoined(from));
                        }
                        PresenceType::Unavailable => {
                            // According to https://xmpp.org/extensions/xep-0045.html#exit, the server will use type "unavailable" to notify the client that it has left the room/
                            events.push(Event::RoomLeft(from));
                        }
                        _ => unimplemented!("Presence type {:?}", presence.type_), // TODO: What to do here?
                    }
                }
            }
        }

        events
    }

    /// Request the avatar (and vCard) of a given Jid.
    ///
    /// As implemented, this requests the XEP-0153 vCard-Based Avatar.
    ///
    /// If successful, this will result in a `PhotoRetrieved` event.
    pub async fn retrieve_avatar(&mut self, jid: &Jid) {
        let iq = Iq::from_get("get_vcard", VCard { photo: None })
            .with_to(jid.clone())
            .with_from(self.client.bound_jid().unwrap().clone());

        if let Err(e) = self.client.send_stanza(iq.into()).await {
            error!("Error while sending Iq stanza: {}", e);
        }
    }

    /// Wait for new events.
    ///
    /// # Returns
    ///
    /// - `Some(events)` if there are new events; multiple may be returned at once.
    /// - `None` if the underlying stream is closed.
    pub async fn wait_for_events(&mut self) -> Option<Vec<Event>> {
        if let Some(event) = self.client.next().await {
            let mut events = Vec::new();

            match event {
                TokioXmppEvent::Online { resumed: false, .. } => {
                    let presence = Self::make_initial_presence(&self.disco, &self.node).into();
                    let _ = self.client.send_stanza(presence).await;
                    events.push(Event::Online);
                    // TODO: only send this when the ContactList feature is enabled.
                    let iq = Iq::from_get(
                        "roster",
                        Roster {
                            ver: None,
                            items: vec![],
                        },
                    )
                    .into();
                    let _ = self.client.send_stanza(iq).await;

                    // Query account disco to know what bookmarks spec is used
                    let iq = Iq::from_get("disco-account", DiscoInfoQuery { node: None }).into();
                    let _ = self.client.send_stanza(iq).await;
                    self.awaiting_disco_bookmarks_type = true;
                }
                TokioXmppEvent::Online { resumed: true, .. } => {}
                TokioXmppEvent::Disconnected(e) => {
                    events.push(Event::Disconnected(e));
                }
                TokioXmppEvent::Stanza(elem) => {
                    if elem.is("iq", "jabber:client") {
                        let iq = Iq::try_from(elem).unwrap();
                        let new_events = self.handle_iq(iq).await;
                        events.extend(new_events);
                    } else if elem.is("message", "jabber:client") {
                        let message = Message::try_from(elem).unwrap();
                        let new_events = self.handle_message(message).await;
                        events.extend(new_events);
                    } else if elem.is("presence", "jabber:client") {
                        let presence = Presence::try_from(elem).unwrap();
                        let new_events = self.handle_presence(presence).await;
                        events.extend(new_events);
                    } else if elem.is("error", "http://etherx.jabber.org/streams") {
                        println!("Received a fatal stream error: {}", String::from(&elem));
                    } else {
                        panic!("Unknown stanza: {}", String::from(&elem));
                    }
                }
            }

            Some(events)
        } else {
            None
        }
    }

    pub async fn upload_file_with(&mut self, service: &str, path: &Path) {
        let name = path.file_name().unwrap().to_str().unwrap().to_string();
        let file = File::open(path).await.unwrap();
        let size = file.metadata().await.unwrap().len();
        let slot_request = SlotRequest {
            filename: name,
            size: size,
            content_type: None,
        };
        let to = service.parse::<Jid>().unwrap();
        let request = Iq::from_get("upload1", slot_request).with_to(to.clone());
        self.uploads
            .push((String::from("upload1"), to, path.to_path_buf()));
        self.client.send_stanza(request.into()).await.unwrap();
    }
}

async fn handle_upload_result(
    from: &Jid,
    iqid: String,
    elem: Element,
    agent: &mut Agent,
) -> impl IntoIterator<Item = Event> {
    let mut res: Option<(usize, PathBuf)> = None;

    for (i, (id, to, filepath)) in agent.uploads.iter().enumerate() {
        if to == from && id == &iqid {
            res = Some((i, filepath.to_path_buf()));
            break;
        }
    }

    if let Some((index, file)) = res {
        agent.uploads.remove(index);
        let slot = SlotResult::try_from(elem).unwrap();

        let mut headers = ReqwestHeaderMap::new();
        for header in slot.put.headers {
            let (attr, val) = match header {
                HttpUploadHeader::Authorization(val) => ("Authorization", val),
                HttpUploadHeader::Cookie(val) => ("Cookie", val),
                HttpUploadHeader::Expires(val) => ("Expires", val),
            };
            headers.insert(attr, val.parse().unwrap());
        }

        let web = ReqwestClient::new();
        let stream = FramedRead::new(File::open(file).await.unwrap(), BytesCodec::new());
        let body = ReqwestBody::wrap_stream(stream);
        let res = web
            .put(slot.put.url.as_str())
            .headers(headers)
            .body(body)
            .send()
            .await
            .unwrap();
        if res.status() == 201 {
            return vec![Event::HttpUploadedFile(slot.get.url)];
        }
    }

    return vec![];
}

#[cfg(test)]
mod tests {
    use super::{Agent, BareJid, ClientBuilder, ClientFeature, ClientType, Event};
    use std::str::FromStr;
    use tokio_xmpp::AsyncClient as TokioXmppClient;

    #[tokio::test]
    async fn test_simple() {
        let jid = BareJid::from_str("foo@bar").unwrap();

        let client = TokioXmppClient::new(jid.clone(), "meh");

        // Client instance
        let client_builder = ClientBuilder::new(jid, "meh")
            .set_client(ClientType::Bot, "xmpp-rs")
            .set_website("https://gitlab.com/xmpp-rs/xmpp-rs")
            .set_default_nick("bot")
            .enable_feature(ClientFeature::ContactList);

        #[cfg(feature = "avatars")]
        let client_builder = client_builder.enable_feature(ClientFeature::Avatars);

        let mut agent: Agent = client_builder.build_impl(client);

        while let Some(events) = agent.wait_for_events().await {
            assert!(match events[0] {
                Event::Disconnected(_) => true,
                _ => false,
            });
            assert_eq!(events.len(), 1);
            break;
        }
    }
}
